package com.basededatos15.repository;

import com.basededatos15.cotizacion.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestorCotizacionRepository {

    private String urlLocal = "jdbc:mysql://localhost:3306/BaseLocal";
    private String usuario = "usuario";
    private String password = "caperusita20";

    public GestorCotizacionRepository() {
    }

    public String getUrlLocal() {
        return urlLocal;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUrlLocal(String urlLocal) {
        this.urlLocal = urlLocal;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Cotizacion obtenerUltimaCotizacion(String nombreProveedor, String monedaProveedor) {
        Date fechaDate = null;
        String moneda = null;
        double precio = 0;
        Cotizacion cotizacion = null;
        try (Connection coneccion = DriverManager.getConnection(urlLocal, usuario, password); Statement estado = coneccion.createStatement()) {
            ResultSet resultado = estado.executeQuery("select * from cotizacion_historico where proveedor = '"
                    + nombreProveedor + "' and moneda = \"" + monedaProveedor + "\" and fecha <= NOW() order by fecha desc limit 0,1;");
            while (resultado.next()) {
                fechaDate = resultado.getTimestamp("Fecha");
                moneda = resultado.getString("Moneda");
                precio = resultado.getDouble("Precio");
            }
            cotizacion = new Cotizacion(nombreProveedor, fechaDate, moneda, precio);
            coneccion.close();
            estado.close();
        } catch (SQLException ex) {
            cotizacion = new Cotizacion(nombreProveedor, null, null, -1);
        }
        return cotizacion;
    }

    public void guardarCotizacion(Cotizacion cotizacion) {
        try (Connection conexion = DriverManager.getConnection(urlLocal, usuario, password)) {
            if (!cotizacion.getNombreProveedor().equals("FinanSur")) {
                String query = "insert into cotizacion_historico (Proveedor, Fecha, Moneda, Precio) values (?, ?, ?, ?)";
                PreparedStatement estado = conexion.prepareStatement(query);
                estado.setString(1, cotizacion.getNombreProveedor());
                estado.setString(2, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cotizacion.getFecha()));
                estado.setString(3, cotizacion.getMoneda());
                estado.setString(4, Double.toString(cotizacion.getPrecio()));
                estado.executeUpdate();
                estado.close();
            }
            conexion.close();
        } catch (SQLException ex) {
            System.out.println( ex.getMessage());
        }
    }
}
