package com.basededatos15.repository;

import com.basededatos15.cotizacion.Cotizacion;

public abstract class CotizacionRepository {

    public abstract String getNombreProveedor();

    public abstract Cotizacion obtenerCotizacion();
    
    public abstract String getMoneda();
}
