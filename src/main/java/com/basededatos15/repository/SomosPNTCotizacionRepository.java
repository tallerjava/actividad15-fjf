package com.basededatos15.repository;

import com.basededatos15.cotizacion.Cotizacion;
import java.sql.SQLException;
import java.util.Date;
import jodd.http.HttpException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class SomosPNTCotizacionRepository extends CotizacionRepository {

    private String url = "http://dev.somospnt.com:9756/quote";
    private String nombreProveedor = "SomosPNT";
    private String moneda = "USD";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
            HttpResponse response = HttpRequest.get(url).send();
            JSONObject body = new JSONObject(response.body());
            Date fecha = new Date();
            double precio = body.getDouble("price");
            Cotizacion cotizacion = new Cotizacion(nombreProveedor, fecha, moneda, precio);
            return cotizacion;
    }
}
