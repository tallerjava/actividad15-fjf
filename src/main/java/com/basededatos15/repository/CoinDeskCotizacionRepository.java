package com.basededatos15.repository;

import com.basededatos15.cotizacion.Cotizacion;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CoinDeskCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";
    private String nombreProveedor = "CoinDesk";
    private String moneda = "USD";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }
    
    @Override
    public String getMoneda() {
        return moneda;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        Date fecha = new Date();
        String moneda2 = body.getJSONObject("bpi").getJSONObject(moneda).getString("code");
        double precio = body.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
        Cotizacion cotizacion = new Cotizacion(nombreProveedor, fecha, moneda2, precio);
        return cotizacion;
    }
}
