package com.basededatos15.app;

import com.basededatos15.cotizacionservice.CotizacionService;
import com.basededatos15.cotizacion.Cotizacion;
import com.basededatos15.repository.*;
import java.net.ConnectException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws SQLException, ConnectException {

        System.out.println("Pulse la tecla Enter para obtener la ultima cotizacion del dia");
        Scanner tecla = new Scanner(System.in);
        String enterKey = tecla.nextLine();
        String receivedKey = null;
        do {
            if (enterKey.isEmpty()) {
                ArrayList<CotizacionRepository> cotizacionRepositoryListado = new ArrayList();
                cotizacionRepositoryListado.add(new CoinDeskCotizacionRepository());
                cotizacionRepositoryListado.add(new BinanceCotizacionRepository());
                cotizacionRepositoryListado.add(new CryptoCompareCotizacionRepository());
                cotizacionRepositoryListado.add(new SomosPNTCotizacionRepository());
                cotizacionRepositoryListado.add(new FinanSurCotizacionRepository());
                CotizacionService cotizacionService = new CotizacionService(cotizacionRepositoryListado);
                ArrayList<Cotizacion> cotizacionListado = cotizacionService.obtenerListadoCotizacion();
                for (Cotizacion cotizacion : cotizacionListado) {
                    if (cotizacion.getPrecio() > -1) {
                        System.out.println(cotizacion);
                    } else {
                        System.out.println(cotizacion.getNombreProveedor() + ":\t(El proveedor no se encuentra disponible)");
                    }
                }
                System.out.println("(Presione Enter para volver a consultar)");
                receivedKey = tecla.nextLine();
            }
        } while (receivedKey != null);

    }
}
