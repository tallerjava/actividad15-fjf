package com.basededatos15.cotizacionservice;

import com.basededatos15.cotizacion.Cotizacion;
import com.basededatos15.repository.CotizacionRepository;
import com.basededatos15.repository.GestorCotizacionRepository;
import java.util.ArrayList;

public class CotizacionService {

    private ArrayList<CotizacionRepository> cotizacionRepositoryListado;

    public CotizacionService(ArrayList<CotizacionRepository> cotizacionRepositoryListado) {
        this.cotizacionRepositoryListado = cotizacionRepositoryListado;
    }
    public ArrayList<Cotizacion> obtenerListadoCotizacion() {
        ArrayList<Cotizacion> cotizacionListado = new ArrayList();
        for (CotizacionRepository cotizacionRepository : cotizacionRepositoryListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            String moneda = cotizacionRepository.getMoneda();
            GestorCotizacionRepository GestorCotizacionRepository = new GestorCotizacionRepository();
            try {
                cotizacionListado.add(cotizacionRepository.obtenerCotizacion());
                GestorCotizacionRepository.guardarCotizacion(cotizacionRepository.obtenerCotizacion());
            } catch (Exception ex) {
                try {
                    Cotizacion cotizacionAlterna = GestorCotizacionRepository.obtenerUltimaCotizacion(nombreProveedor, moneda);
                    cotizacionListado.add(cotizacionAlterna);
                } catch (Exception e) {
                    cotizacionListado.add(new Cotizacion(cotizacionRepository.getNombreProveedor(), null, null, -1));
                }
            }
        }
        return cotizacionListado;
    }
}

