    
//import java.net.ConnectException;
import com.basededatos15.cotizacionservice.CotizacionService;
import com.basededatos15.repository.CoinDeskCotizacionRepository;
import com.basededatos15.repository.CotizacionRepository;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    public CotizacionServiceTest() {
    }

    @Test(expected = NullPointerException.class)
    public void obtenerListadoCotizacion_ArgumentoArrayCotizacionRepositoryNull_NullPointerException(){
        ArrayList<CotizacionRepository> listadoCotizacionRepository = null;
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacionRepository);
        cotizacionService.obtenerListadoCotizacion();
    }

    @Test
    public void obtenerListadoCotizacion_ArgumentoValidoArrayCotizacionRepository_listadoObtenido(){
        ArrayList<CotizacionRepository> listadoCotizacionRepository = new ArrayList();
        listadoCotizacionRepository.add(new CoinDeskCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacionRepository);
        assertNotNull(cotizacionService.obtenerListadoCotizacion());
    }
}
