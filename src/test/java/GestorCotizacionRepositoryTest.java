//
import com.basededatos15.cotizacion.Cotizacion;
import com.basededatos15.repository.GestorCotizacionRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class GestorCotizacionRepositoryTest {

    @Test
    public void obtenerUltimaCotizacion_urlInvalida_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUrlLocal("https://api.coindesk.com/v1/bpi/currentprice.xml");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerUltimaCotizacion_urlNull_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUrlLocal(null);
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerUltimaCotizacion_usuarioInvalido_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUsuario("usuarioInvalido");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerUltimaCotizacion_passwordInvalido_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setPassword("usuarioInvalido");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void obtenerUltimaCotizacion_condicionesValidas_ultimaCotizacion() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        boolean resultadoEsperado = true;
        boolean resultadoObtenido = false;
        Double precio = coti.getPrecio();
        if (precio >= 0) {
            resultadoObtenido = true;
        }
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardarCotizacion_urlInvalida_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUrlLocal("https://api.coindesk.com/v1/bpi/currentprice.xml");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardarCotizacion_urlNull_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUrlLocal(null);
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardarCotizacion_usuarioInvalido_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setUsuario("usuarioInvalido");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardarCotizacion_passwordInvalido_cotizacionConPrecioCero() {
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        gestorCotizacion.setPassword("usuarioInvalido");
        Cotizacion coti = gestorCotizacion.obtenerUltimaCotizacion("Finansur", "USD");
        Double resultadoEsperado;
        resultadoEsperado = -1.00;
        Double resultadoObtenido = coti.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void guardarCotizacion_condicionesValidas_cotizacionGuardada() throws ParseException, SQLException {
        int ultimoIdCotizacion = 0;
        int nuevoIdCotizacion = 0;
        Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/BaseLocal", "usuario", "caperusita20");
        Statement estado = conexion.createStatement();
        ResultSet resultado = estado.executeQuery("select * from cotizacion_historico order by Cotizacion_ID desc limit 0,1;");
        resultado.next();
        ultimoIdCotizacion = resultado.getInt("Cotizacion_ID");
        GestorCotizacionRepository gestorCotizacion = new GestorCotizacionRepository();
        String sFecha = "2018-03-08 17:47:28";
        Date fecha = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(sFecha);
        Cotizacion cotizacion = new Cotizacion("Finansur", fecha, "USD", 100.01);
        gestorCotizacion.guardarCotizacion(cotizacion);
        resultado = estado.executeQuery("select * from cotizacion_historico order by Cotizacion_ID desc limit 0,1;");
        resultado.next();
        nuevoIdCotizacion = resultado.getInt("Cotizacion_ID");
        ultimoIdCotizacion = ultimoIdCotizacion + 1;
        conexion.close();
        estado.close();
        assertEquals(ultimoIdCotizacion, nuevoIdCotizacion);
    }
}
