
import com.basededatos15.repository.SomosPNTCotizacionRepository;
import jodd.http.HttpException;
import org.json.JSONException;
import org.junit.Test;
import static org.junit.Assert.*;

public class SomosPNTCotizacionRepositoryTest {

    public SomosPNTCotizacionRepositoryTest() {
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_sinUrl_httpException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("");
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = JSONException.class)
    public void obtenerCotizacion_urlInvalida_JSONException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_urlNull_NullPointerException() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        somosPNTCotizacionRepository.setUrl(null);
        somosPNTCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        SomosPNTCotizacionRepository somosPNTCotizacionRepository = new SomosPNTCotizacionRepository();
        assertNotNull(somosPNTCotizacionRepository.obtenerCotizacion());
    }    
}
