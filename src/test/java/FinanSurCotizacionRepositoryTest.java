
import com.basededatos15.cotizacion.Cotizacion;
import com.basededatos15.repository.FinanSurCotizacionRepository;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    public FinanSurCotizacionRepositoryTest() {
    }

    @Test
    public void obtenerCotizacion_urlInvalida_cotizacionNull() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal("sdaasd");
        Cotizacion cotizacion = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null; 
        assertEquals(resultadoEsperado, cotizacion);
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        assertNotNull(finanSurCotizacionRepository.obtenerCotizacion());
    }

    @Test
    public void obtenerCotizacion_urlNull_nullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal(null);
        Cotizacion cotizacion = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null; 
        assertEquals(resultadoEsperado, cotizacion);
    }

    @Test
    public void obtenerCotizacion_usuarioInvalido_nullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUsuario("otroUser");
        Cotizacion cotizacion = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null; 
        assertEquals(resultadoEsperado, cotizacion);
    }

    @Test
    public void obtenerCotizacion_passwordInvalido_nullPointerException() {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setPassword("nuevaContr4");
        Cotizacion cotizacion = finanSurCotizacionRepository.obtenerCotizacion();
        Cotizacion resultadoEsperado = null; 
        assertEquals(resultadoEsperado, cotizacion);
    }
}